import java.io.IOException;
import java.lang.*;
import java.nio.file.Paths;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.document.*;
import java.nio.file.Path;
import java.sql.*;


public class Searcher {
    final static String LUCENE_TABLE = "lucene_output";
    final static String BOUNDING_BOX_NAME = "poly";
    final static String NEIGHBOURS_IN_BOX_TABLE = "neighbours_in_box";
    final static String NEIGHBOUR_TABLE = "neighbours";
    final static String RANKING_TABLE = "ranking";
    final static String NO_OUTPUT = "No output to display!";
    final static String KM_PER_DEGREE_LAT = "111.2";
    final static double RADIUS_OF_EARTH = 6371;
    static class Point {
        Double latitude;
        Double longitude;

        public Point(Double latitude, Double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }
    }
    public Searcher() {}

    public static void main(String[] args) {
        String usage = "java Searcher";
        if(args.length != 1 && args.length != 7){
            System.out.println("Incorrect number of arguments");
        } else {
            TopDocs lucene_output = searchKeywords(args[0]);
            if(lucene_output.totalHits == 0){
                System.out.println(NO_OUTPUT);
                return;
            }
            if(args.length == 1){
                printResults(lucene_output);
            } else {
                // Longitude comes first in input but this is not the standard so from this point latitude always comes first
                Point home = new Point(Double.parseDouble(args[4]), Double.parseDouble(args[2]));
                printFilteredByDistance(lucene_output, home, Double.parseDouble(args[6]));

            }
        }
    }

    private static void printFilteredByDistance(TopDocs docs, Point home, Double max_distance){

        Connection conn;
        try {
            conn = DbManager.getConnection(false);
            build_lucene_table(docs, conn);
            build_table_of_neighbours(home, max_distance, conn);
            join_lucene_and_neighbour_tables(conn);
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private static void join_lucene_and_neighbour_tables(Connection conn) throws SQLException{
        Statement table_creation_statement = conn.createStatement();
        String table_creation_query = String.format("CREATE TEMPORARY TABLE %s AS " +
                                                    "(SELECT i.item_id, distance, price, item_name, lucene_score FROM " +
                                                    "item i INNER JOIN %s j ON i.item_id = j.item_id " +
                                                    "INNER JOIN %s d ON i.item_id = d.item_id) ;",
                                                    RANKING_TABLE,
                                                    LUCENE_TABLE,
                                                    NEIGHBOUR_TABLE);
        table_creation_statement.execute(table_creation_query);
        Statement counting_statement = conn.createStatement();
        String counting_query = String.format("SELECT COUNT(*) AS count FROM %s;", RANKING_TABLE);
        ResultSet counter = counting_statement.executeQuery(counting_query);
        while(counter.next()){
            int count = counter.getInt("count");
            if(count == 0){
                System.out.println(NO_OUTPUT);
                return;
            } else {
                System.out.println(String.format("totalHits: %d",count));
            }
            break;
        }



        Statement ranking_statement = conn.createStatement();
        String ranking_query = String.format("SELECT item_id, distance, price, item_name, lucene_score FROM %s " +
                                            "ORDER BY lucene_score DESC, distance ASC, price ASC;",
                                            RANKING_TABLE);
        ResultSet rows = ranking_statement.executeQuery(ranking_query);
        while(rows.next()){
            String outString = String.format("%s, %s, price: %.2f, score: %.5f, distance: %.2f",
                                            rows.getString("item_id"),
                                            rows.getString("item_name"),
                                            rows.getDouble("price"),
                                            rows.getDouble("lucene_score"),
                                            rows.getDouble("distance"));
            System.out.println(outString);
        }

    }
    private static void undefine_distance_function(Connection conn) throws SQLException{
        Statement undefine_function = conn.createStatement();
        undefine_function.execute("DROP FUNCTION IF EXISTS km_dist_between_points;");
    }
    private static void define_distance_function(Connection conn) throws SQLException{
        Statement define_function_stmt = conn.createStatement();
        String define_function_query = String.format("CREATE FUNCTION km_dist_between_points(" +
                                        "lat1 DECIMAL(10,6), long1 DECIMAL(10,6), " +
                                        "lat2 DECIMAL(10,6), long2 DECIMAL(10,6)) " +
                                        "RETURNS DECIMAL(10,3) DETERMINISTIC " +
                "return ( DEGREES( ACOS( COS(RADIANS(lat1)) * COS(RADIANS(lat2)) * " +
                "COS( RADIANS(long1)- RADIANS(long2)) +" +
                "SIN(RADIANS(lat1)) * SIN(RADIANS(lat2)) ) ) * %s) ;", KM_PER_DEGREE_LAT);
        define_function_stmt.execute(define_function_query);
    }
    private static void build_table_of_neighbours(Point home, double max_distance, Connection conn) throws SQLException{
        // along with build_bounding_box this method first builds a bounding box to shrink the number of points
        // we need to examine and then searches within this bounding box for neighbours within max_distance of home.
        Statement table_creation_statement = conn.createStatement();
        String bounding_box_query = build_bounding_box(home, max_distance);
        
        String table_creation_query = String.format("CREATE TEMPORARY TABLE %s AS " +
                        "(SELECT item_id, xy FROM item_points " +
                        "WHERE MBRWithin(xy, @%s));"  ,
                NEIGHBOURS_IN_BOX_TABLE,
                BOUNDING_BOX_NAME);
        table_creation_statement.addBatch(bounding_box_query);
        table_creation_statement.addBatch(table_creation_query);
        define_distance_function(conn);
        String nearest_neighbours_creation_query = String.format("CREATE TEMPORARY TABLE %s AS " +
                                                                "(SELECT item_id, km_dist_between_points(%s, %s, X(xy), Y(xy)) AS distance FROM %s " +
                                                                "WHERE km_dist_between_points(%s, %s, X(xy), Y(xy)) <= %s );",
                                                                    NEIGHBOUR_TABLE,
                                                                    home.latitude, home.longitude,
                                                                    NEIGHBOURS_IN_BOX_TABLE,
                                                                    home.latitude, home.longitude,
                                                                    max_distance);
        table_creation_statement.addBatch(nearest_neighbours_creation_query);
        table_creation_statement.executeBatch();
        undefine_distance_function(conn);

    }

    private static String build_bounding_box(Point home, double max_distance){
        //initial box calculations
        //if we cover a pole then we take a 'hemisphere' (not necessarily actually a hemisphere
        // but we take the entire earth above a circle).
        double latitude_offset = (max_distance / RADIUS_OF_EARTH) * (180 / Math.PI);
        double upperLat = home.latitude + latitude_offset;
        upperLat = upperLat > 90 ? 90 : upperLat;
        double lowerLat = home.latitude - latitude_offset;
        lowerLat = lowerLat < -90 ? -90 : lowerLat;

        double lowerLong, upperLong;
        if(upperLat == 90 || lowerLat == -90){
            lowerLong = -180;
            upperLong = 180;
        } else {
            double longitude_offset = latitude_offset / (  Math.cos(Math.toRadians(home.latitude)) );

            upperLong = home.longitude + longitude_offset;
            lowerLong = home.longitude - longitude_offset;
        }

        // build second box if we go over one edge but not the other
        if((upperLong > 180 && lowerLong > -180 )  || (lowerLong < -180 && upperLong < 180)){
            double upperLong2, lowerLong2;
            if(upperLong > 180 && lowerLong > -180){
                upperLong2 = upperLong - 360;
                lowerLong2 = lowerLong - 360;
            } else{
                upperLong2 = upperLong + 360;
                lowerLong2 = lowerLong + 360;
            }

            return String.format("SET @%s = GeomFromText('MultiPolygon( (( %.5f %.5f, %.5f %.5f, %.5f %.5f, %.5f %.5f, %.5f %.5f)), " +
                            " ((%.5f %.5f, %.5f %.5f, %.5f %.5f, %.5f %.5f, %.5f %.5f)) ) '); ",
                    BOUNDING_BOX_NAME,
                    lowerLat, lowerLong,
                    lowerLat, upperLong,
                    upperLat, upperLong,
                    upperLat, lowerLong,
                    lowerLat, lowerLong,

                    lowerLat, lowerLong2,
                    lowerLat, upperLong2,
                    upperLat, upperLong2,
                    upperLat, lowerLong2,
                    lowerLat, lowerLong2
                    );
        } else {
            return String.format("SET @%s = GeomFromText('Polygon( (%.5f %.5f, %.5f %.5f, %.5f %.5f, %.5f %.5f, %.5f %.5f) )'); ",
                    BOUNDING_BOX_NAME,
                    lowerLat, lowerLong,
                    lowerLat, upperLong,
                    upperLat, upperLong,
                    upperLat, lowerLong,
                    lowerLat, lowerLong );
        }


    }
    private static void build_lucene_table(TopDocs docs, Connection conn) throws IOException, SQLException{
        // This method builds a table that stores the lucene scores of the items for this search

        IndexSearcher indexSearcher = setupIndex("indexes");
        Statement table_creation_stmt = conn.createStatement();
        String table_creation_query = String.format("CREATE TEMPORARY TABLE %s (item_id INT(11) PRIMARY KEY, " +
                "lucene_score DOUBLE," +
                "price DOUBLE " +
                ");", LUCENE_TABLE);
        table_creation_stmt.addBatch(table_creation_query);
        String table_insert_query = String.format("INSERT INTO %s (item_id, price, lucene_score) VALUES ", LUCENE_TABLE);
        int count = 0;
        for(ScoreDoc scoreDoc: docs.scoreDocs){
            Document document = indexSearcher.doc(scoreDoc.doc);
            table_insert_query += String.format("(%s,%s, %s)",
                    document.get("item_id"),
                    document.get("price"),
                    scoreDoc.score);
            count ++;
                if(count == docs.totalHits){
                    table_insert_query+= ";";
                } else {
                    table_insert_query += ",";
                }
        }
        table_creation_stmt.addBatch(table_insert_query);
        table_creation_stmt.executeBatch();
    }
    private static IndexSearcher setupIndex(String p) {
        try {
            Path path 					= Paths.get(p);
            Directory directory 		= FSDirectory.open(path);
            IndexReader indexReader 	= DirectoryReader.open(directory);
            IndexSearcher indexSearcher = new IndexSearcher(indexReader);

            return indexSearcher;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static TopDocs searchKeywords(String searchText) {
        IndexSearcher indexSearcher = setupIndex("indexes");
        System.out.println("Running search(" + searchText + ")");

        try {
            // MultiFieldQueryParser defaults to searching OR wise through the fields named
            QueryParser queryParser = new MultiFieldQueryParser(new String[] {"item_name", "category_name", "description"}, new SimpleAnalyzer());
            // parse defaults to splitting up all the tokens and searching for each one on its own
            Query query 			= queryParser.parse(searchText);
            TopDocs topDocs 		= indexSearcher.search(query,
                    10000,
                    new Sort( SortField.FIELD_SCORE, new SortField("sort_price", SortField.Type.DOUBLE) ),
                    true,
                    false );
            return topDocs;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    private static void printResults(TopDocs topDocs) {
        IndexSearcher indexSearcher = setupIndex("indexes");
        try {
            System.out.println("totalHits " + topDocs.totalHits);
            for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
                Document document = indexSearcher.doc(scoreDoc.doc);
                System.out.println(document.get("item_id") +
                        ", " + document.get("item_name") +
                        ", price: " + document.get("price") +
                        ", score: " + scoreDoc.score);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static void printResults(ResultSet resultSet) {
        if (resultSet == null) {
            System.out.println("No results to display!");
            return;
        }
        try {
            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            int totalNumColumns = resultSetMetaData.getColumnCount();


            while (resultSet.next()) {
                for (int i = 1; i <= totalNumColumns; i++) { // DB tables's coulumn count starts from 1, not 0
                    System.out.print(resultSet.getString(i) + " ");
                }
                System.out.println();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
