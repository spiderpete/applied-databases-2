import java.io.StringReader;
import java.io.File;
import java.nio.file.*;
import java.lang.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.sql.*;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.document.Field.Store;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Indexer {
    public Indexer() {}
    public static IndexWriter indexWriter;
    public static void main(String args[]) {
        String usage = "java Indexer";
        rebuildIndexes("indexes");
    }

    public static void insertDoc(IndexWriter i,
                                 String item_id,
                                 String item_name,
                                 String description,
                                 String categories,
                                 Double price){
        Document doc = new Document();

        //StoredField is a non-indexed stored field so works for ID's which should not be searchable
        doc.add(new StoredField("item_id", item_id));

        // TextField is an indexed, tokenized field type so fits what we need for the other fields
        doc.add(new TextField("description", description ,Field.Store.NO));
        doc.add(new TextField("item_name", item_name ,Field.Store.YES));
        doc.add(new TextField("category_name", categories ,Field.Store.NO));

        // we store the price simply to use it for tie-breaking ordering
        doc.add(new DoubleDocValuesField("sort_price", price));
        // and also as a field for printing later
        doc.add(new DoubleField("price", price, Field.Store.YES));
        try { i.addDocument(doc); } catch (Exception e) { e.printStackTrace(); }
    }

    public static void rebuildIndexes(String indexPath) {
        try {
            Path path = Paths.get(indexPath);
            Directory directory = FSDirectory.open(path);
            IndexWriterConfig config = new IndexWriterConfig(new SimpleAnalyzer());
            IndexWriter index = new IndexWriter(directory, config);
            index.deleteAll();
            // Extracting the rows from SQL and inserting into Lucene
            Connection conn = null;
            Statement stmt = null;
            Statement priceStmt = null;
            try {
                conn = DbManager.getConnection(true);
                stmt = conn.createStatement();

                // GROUP_CONCAT simply concatenates all of the categories that a given item has.
                String item_query = "SELECT i.item_id, item_name, description, current_price AS price, " +
                        "GROUP_CONCAT(category_name SEPARATOR \" \") as categories " +
                        "FROM has_category c INNER JOIN item i ON c.item_id = i.item_id " +
                        "INNER JOIN auction a ON a.item_id = i.item_id " +
                        "GROUP BY i.item_id;";
                ResultSet rows = stmt.executeQuery(item_query);

                while(rows.next()){
                    String item_id, item_name, description, categories;
                    Double price;
                    item_id = rows.getString("item_id");
                    item_name = rows.getString("item_name");
                    description = rows.getString("description");
                    categories = rows.getString("categories");
                    price = rows.getDouble("price");
                    insertDoc(index, item_id,item_name,description, categories, price);
                }
                rows.close();
                conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            index.close();
            directory.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

