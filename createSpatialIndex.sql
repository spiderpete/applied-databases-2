USE ad;

CREATE TABLE IF NOT EXISTS item_points
(
item_id int(11) PRIMARY KEY,
FOREIGN KEY(item_id) REFERENCES item(item_id),
xy POINT NOT NULL
) ENGINE = MyISAM;

CREATE SPATIAL INDEX xy_index ON item_points(xy);

INSERT INTO item_points (item_id, xy)
	SELECT item_id, POINT(latitude, longitude)
	FROM item_coordinates;



